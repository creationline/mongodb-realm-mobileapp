﻿using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using Realms.Sync;
using System.Text.RegularExpressions;

namespace SampleApp
{
    public class SigninFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            // Create your fragment here
            base.OnCreate(savedInstanceState);

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View layout = inflater.Inflate(Resource.Layout.signin, container, false);

            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            // return base.OnCreateView(inflater, container, savedInstanceState);

            return layout;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            AndroidX.Fragment.App.FragmentManager fm = this.ParentFragmentManager;
            App app = ((MainActivity)this.Activity).realmApp;

            if (app.CurrentUser != null)
            {
                Utils.ChangeFragment(this.ParentFragmentManager, new TopFragment());
            }

            // サインアップボタンイベント
            Button signupButton = view.FindViewById<Button>(Resource.Id.MoveToSignupButton);
            signupButton.Click += (sender, e) => Utils.ChangeFragment(fm, new SignupFragment());

            // サインインボタンイベント
            Button signinButton = view.FindViewById<Button>(Resource.Id.SigninButton);
            signinButton.Click += async (sender, e) =>
            {
                signinButton.Enabled = false;

                var eMail = view.FindViewById<EditText>(Resource.Id.EmailText).Text;
                if (!Patterns.EmailAddress.Matcher(eMail).Matches())
                {
                    TextView signinErrorText = view.FindViewById<TextView>(Resource.Id.SigninErrorText);
                    signinErrorText.Text = "メールアドレスの形式で入力してください";
                    signinButton.Enabled = true;
                    return;
                }

                var password = view.FindViewById<EditText>(Resource.Id.PasswordText).Text;
                if (!Regex.IsMatch(password, "^[a-zA-Z0-9]{8,16}$"))
                {
                    TextView signinErrorText = view.FindViewById<TextView>(Resource.Id.SigninErrorText);
                    signinErrorText.Text = "パスワードは8-16文字以内、アルファベットと数字で指定してください";
                    signinButton.Enabled = true;
                    return;
                }

                try
                {
                    await app.LogInAsync(Credentials.EmailPassword(eMail, password));
                    Utils.ChangeFragment(this.ParentFragmentManager, new TopFragment());
                }
                catch (Realms.Sync.Exceptions.AppException)
                {

                    TextView signinErrorText = view.FindViewById<TextView>(Resource.Id.SigninErrorText);
                    signinErrorText.Text = "メールアドレスまたはパスワードが間違っています。";
                    signinButton.Enabled = true;
                }
            };
        }
    }
}