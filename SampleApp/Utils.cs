﻿using Android.Content;
using Android.Widget;
using AndroidX.AppCompat.App;
using AndroidX.Fragment.App;
using System;

namespace SampleApp
{
    internal class Utils
    {
        public static void ChangeFragment(FragmentManager fm, Fragment f) =>
            fm.BeginTransaction()
                .Replace(Resource.Id.MainFragmentContainerView, f)
                .AddToBackStack(null)
                .Commit();

        public static void ShowPopup(Context context, string title, EventHandler<DialogClickEventArgs> e)
        {
            AlertDialog.Builder builderErorr = new AlertDialog.Builder(context);
            builderErorr.SetTitle(title);
            builderErorr.SetPositiveButton("OK", e);
            builderErorr.Show();
        }

        public static bool ValidateForm(out int price, out int discount, object button,
            EditText itemText, EditText priceText, EditText discountText)
        {
            bool result = true;
            price = 0; discount = 0;

            if (itemText.Text == string.Empty)
            {
                SetErrorText(itemText, "文字を入力してください");
            }
            else if (!int.TryParse(priceText.Text, out price))
            {
                SetErrorText(priceText, "整数を入力してください");
            }
            else if (price <= 0)
            {
                SetErrorText(priceText, "1 以上の数字を入力してください");
            }
            else if (!int.TryParse(discountText.Text, out discount))
            {
                SetErrorText(discountText, "整数を入力してください");
            }
            else if (discount > 100 || discount < 0)
            {
                SetErrorText(discountText, "0 ～ 100 までの数字を入力してください");
            }

            ((Button)button).Enabled = !result;
            return result;

            void SetErrorText(EditText editText, string text)
            {
                editText.Error = text;
                result = false;
            }
        }
    }
}