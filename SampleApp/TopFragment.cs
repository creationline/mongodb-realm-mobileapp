﻿using System;
using System.Linq;
using System.IO;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using MongoDB.Bson;
using Realms;
using Realms.Sync;
using TimeZoneConverter;
using CreationLine.SampleApp.BasicRealmOperation;
using AndroidX.Core.Text;

namespace SampleApp
{
    public class TopFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            // Create your fragment here
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
           return inflater.Inflate(Resource.Layout.top, container, false);
        }

        public override async void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            FragmentManager fm = this.ParentFragmentManager;
            App app = ((MainActivity)this.Activity).realmApp;

            TextView helloText = view.FindViewById<TextView>(Resource.Id.HelloTextView);
            helloText.Text = "Hello. " + app.CurrentUser.Profile.Email;

            // サインインユーザーの属性値 (カスタムデータ) を取得する
            BsonDocument customData = await app.CurrentUser.RefreshCustomDataAsync();
            BsonValue isAdminCustomData = customData?.GetValue("is_admin", false) ?? false;
            bool isAdmin = (isAdminCustomData.BsonType is BsonType.Boolean ? isAdminCustomData.AsBoolean : false);

            helloText.Text += isAdmin switch
            {
                true => "\n管理者",
                false => "\n一般ユーザー"
            };

            Button signOutButton = view.FindViewById<Button>(Resource.Id.SignOutButton);
            Button addButtion = view.FindViewById<Button>(Resource.Id.AddButtion);

            // 一般ユーザーのときは追加ボタンを隠す
            addButtion.Visibility = isAdmin switch
            {
                true => ViewStates.Visible,
                false => ViewStates.Gone
            };
            // Realm から商品の一覧を取得する
            var config = new FlexibleSyncConfiguration(app.CurrentUser, "public.realm")
            {
                PopulateInitialSubscriptions = (realm) =>
                {
                    var myInventory = realm.All<Inventory>();
                    realm.Subscriptions.Add(myInventory);
                }
            };
            using Realm publicRealm = await Realm.GetInstanceAsync(config);

            var list = publicRealm.All<Inventory>().OrderByDescending(i => i.Timestamp).ToList();
            TextView inventroyListTitle = view.FindViewById<TextView>(Resource.Id.InventoryListTitle);
            inventroyListTitle.Text += $"({list.Count})";

            ListView listView = view.FindViewById<ListView>(Resource.Id.InventoryListView);
            listView.ItemClick += OnListItemClick;
            var ad = new ArrayAdapter<string>(Activity, Android.Resource.Layout.SimpleListItemActivated1);
            list.ForEach(c => ad.Add((Java.Lang.Object)HtmlCompat.FromHtml(
                $"商品名: {c.Item} {NewIcon(c.Timestamp.DateTime)}<br>"
                + $"定価: {c.Price} 円 => 値引後: {Math.Ceiling(((float)c.Price) * (100 - c.DiscountRate) / 100)} 円<br>"
                + $"値引き率: {c.DiscountRate} % <br>" 
                + $"最終更新: {ToJst(c.Timestamp.DateTime)}", HtmlCompat.FromHtmlModeCompact)));
            listView.Adapter = ad;

            // ボタンを押したときに商品追加画面に遷移する
            addButtion.Click += (sender, e) => Utils.ChangeFragment(this.ParentFragmentManager, new AddFragment());

            // サインアウトボタンを押したときにサインアウトする
            signOutButton.Click += async (sender, e) =>
            {
                await app.RemoveUserAsync(app.CurrentUser);
                Utils.ChangeFragment(this.ParentFragmentManager, new SigninFragment());
            };
        }

        // 商品リストをクリックしたときに編集画面に遷移する
        public async void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            // サインインユーザーの属性値 (カスタムデータ) を取得する。
            App app = ((MainActivity)this.Activity).realmApp;
            BsonDocument customData = await app.CurrentUser.RefreshCustomDataAsync();
            BsonValue isAdminCustomData = customData?.GetValue("is_admin", false) ?? false;
            bool isAdmin = (isAdminCustomData.BsonType is BsonType.Boolean ? isAdminCustomData.AsBoolean : false);
            if (isAdmin)
            {
                using Realm publicRealm = await Realm.GetInstanceAsync(
                    new FlexibleSyncConfiguration(app.CurrentUser, "public.realm"));

                var list = publicRealm.All<Inventory>().OrderByDescending(i => i.Timestamp).ToList();
                EditFragment ef = new EditFragment();
                Bundle bundle = new Bundle();
                bundle.PutString("mongo_id", $"{list[e.Position].Id}");
                bundle.PutString("item", $"{list[e.Position].Item}");
                bundle.PutString("price", $"{list[e.Position].Price}");
                bundle.PutString("discount_rate", $"{list[e.Position].DiscountRate}");
                ef.Arguments = bundle;
                Utils.ChangeFragment(this.ParentFragmentManager, ef);
            }
        }

        public DateTime ToJst(DateTime dateTime)
        {
            var jstZoneInfo = TZConvert.GetTimeZoneInfo("Tokyo Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, jstZoneInfo);
        }
        public string NewIcon(DateTime dateTime)
        {
            var dataTime = ToJst(dateTime);
            TimeSpan timeSpan = ToJst(DateTimeOffset.Now.DateTime) - dataTime;
            if (timeSpan.TotalMinutes < 30)
            {
                return "<font color=#ff0000>New!!</font>";
            }
            else
            {
                return "";
            }
        }
    }
}