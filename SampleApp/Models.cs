﻿using System;
using Java.IO;
using MongoDB.Bson;
using Realms;

namespace CreationLine.SampleApp.BasicRealmOperation
{
    /// <summary>
    /// Computer モデル。
    /// </summary>
    /// <remarks>
    /// Realm アプリに接続して同期連携するには、Realm DB に _id と _partition という
    /// 2 つのプロパティが必要になります。<br />
    /// C# では Id と Partition という名前で表現し、MapTo 属性でマッピングしています。<br />
    /// ＜属性について＞<br />
    /// - PrimaryKey: このプロパティは主キーである。<br />
    /// - MapTo(mapping): DB と C# モデルで名前が違う場合のマッピング。<br />
    /// - Required: このプロパティには、必ず値を指定しなければならない。
    /// </remarks>
    public class Computer : RealmObject
    {
        /// <summary>
        /// データを特定する一意の値であり、主キーになります。
        /// </summary>
        /// <remarks>
        /// ObjectId.GenerateNewId() で ID を自動生成します。
        /// </remarks>
        [PrimaryKey]
        [MapTo("_id")]
        public ObjectId Id { get; set; } = ObjectId.GenerateNewId();

        /// <summary>
        /// このデータが属するパーティションの名前が入ります。
        /// </summary>
        [MapTo("_partition")]
        [Required]
        public string Partition { get; set; }

        [MapTo("name")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Cpu モデルと 1 対 1 の関係になります。
        /// </summary>
        [MapTo("cpu")]
        public Cpu Cpu { get; set; }

        [MapTo("memory")]
        public int Memory { get; set; }

        [MapTo("owner")]
        public string Owner { get; set; }

        private static readonly Random s_random = new Random();

        /// <summary>
        /// 新しい Computer クラスのインスタンスを作成します。
        /// </summary>
        /// <remarks>
        /// Memory と、Cpu の Frequency, Core はランダムな数値になり、<br />
        /// 作成するインスタンスごとに異なった値になります。
        /// </remarks>
        public static Computer Create(string partition, string name, string owner) =>
            new Computer()
            {
                Partition = partition,
                Name = name,
                Owner = owner,
                Memory = s_random.Next(1, 128),
                Cpu = new Cpu
                {
                    Frequency = Math.Round(s_random.NextDouble() * 1000, 1),
                    Core = s_random.Next(1, 32)
                }
            };

        public override string ToString() =>
            $"Id:{this.Id}, Partition:{this.Partition}, Name:{this.Name}, " +
            $"CPU:({this.Cpu}), Memory:{this.Memory}, Owner:{this.Owner}";
    }


    /// <summary>
    /// Cpu モデル。
    /// </summary>
    /// <remarks>
    /// 親モデルは Computer になります。<br />
    /// 親モデルに Partition プロパティが定義されているため、このモデルには不要です。
    /// </remarks>
    public class Cpu : RealmObject
    {
        [PrimaryKey]
        [MapTo("_id")]
        public ObjectId Id { get; set; } = ObjectId.GenerateNewId();

        [MapTo("frequency")]
        public double Frequency { get; set; }

        [MapTo("core")]
        public int Core { get; set; }

        public override string ToString() => $"Frequency:{this.Frequency}, Core:{this.Core}";
    }

    /// <summary>
    /// Inventory モデル。
    /// </summary>
    /// <remarks>
    /// スーパーのセール情報を扱うモデルです。
    /// </remarks>
    public class Inventory : RealmObject
    {
        [PrimaryKey]
        [MapTo("_id")]
        public ObjectId Id { get; set; } = ObjectId.GenerateNewId();

        [MapTo("_partition")]
        [Required]
        public string Partition { get; set; }
        /// <summary>
        /// 商品名
        /// </summary>
        [MapTo("item")]
        [Required]
        public string Item { get; set; }
        /// <summary>
        /// 商品の定価
        /// </summary>
        [MapTo("price")]
        public int Price { get; set; }
        /// <summary>
        ///　商品の割引率
        /// </summary>
        [MapTo("discount_rate")]
        public int DiscountRate { get; set; }
        /// <summary>
        /// 商品の最終更新時間
        /// </summary>
        [MapTo("timestamp")]
        public DateTimeOffset Timestamp { get; set; } = DateTimeOffset.Now;
    }
}
