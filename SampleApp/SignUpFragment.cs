﻿using System.Text.RegularExpressions;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using Realms.Sync;

namespace SampleApp
{
    public class SignupFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            // Create your fragment here
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View layout = inflater.Inflate(Resource.Layout.signup, container, false);
            return layout;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            FragmentManager fm = this.ParentFragmentManager;

            Button signupButton = view.FindViewById<Button>(Resource.Id.SignupButton);
            signupButton.Click += async (sender, e) =>
            {
                // サインアップボタンを非活性にする
                signupButton.Enabled = false;

                var eMail = view.FindViewById<EditText>(Resource.Id.EmailText).Text;
                if (!Patterns.EmailAddress.Matcher(eMail).Matches())
                {
                    TextView SignupErrorText = view.FindViewById<TextView>(Resource.Id.SignupErrorText);
                    SignupErrorText.Text = "メールアドレスの形式で入力してください";
                    signupButton.Enabled = true;
                    return;
                }

                var password = view.FindViewById<EditText>(Resource.Id.PasswordText).Text;
                if (!Regex.IsMatch(password, "^[a-zA-Z0-9]{8,16}$"))
                {
                    TextView signupErrorText = view.FindViewById<TextView>(Resource.Id.SignupErrorText);
                    signupErrorText.Text = "パスワードは8-16文字以内、アルファベットと数字で指定してください";
                    signupButton.Enabled = true;
                    return;
                }

                var termsOfSvcCheckbox = view.FindViewById<CheckBox>(Resource.Id.TermsOfSvcCheckBox);
                if (!termsOfSvcCheckbox.Checked)
                {
                    TextView signupErrorText = view.FindViewById<TextView>(Resource.Id.SignupErrorText);
                    signupErrorText.Text = "利用規約にチェックを入れてください";
                    signupButton.Enabled = true;
                    return;
                }

                App app = ((MainActivity)this.Activity).realmApp;

                try
                {
                    await app.EmailPasswordAuth.RegisterUserAsync(eMail, password);
                    // テキストの案内を表示する
                    view.FindViewById<TextView>(Resource.Id.ResultText).Text = "メール送ったでー";
                }
                catch (Realms.Sync.Exceptions.AppException ex)
                {
                    TextView signupErrorText = view.FindViewById<TextView>(Resource.Id.SignupErrorText);

                    if (ex.StatusCode == System.Net.HttpStatusCode.Conflict)
                    {
                        signupErrorText.Text = "サインアップに失敗しました";
                    }
                    else
                    {
                        signupErrorText.Text = "サインアップエラー";
                    }
                }
                signupButton.Enabled = true;
            };

            Button moveToSigninButton = view.FindViewById<Button>(Resource.Id.MoveToSigninButton);
            moveToSigninButton.Click += (sender, e) =>
                 Utils.ChangeFragment(this.ParentFragmentManager, new SigninFragment());
        }
    }
}