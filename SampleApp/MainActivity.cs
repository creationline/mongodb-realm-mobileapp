﻿using System;
using System.IO;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Views;
using AndroidX.AppCompat.App;
using Google.Android.Material.Snackbar;
using Realms.Sync;

namespace SampleApp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        public Realms.Sync.App realmApp;
        public readonly string realmDir = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "Realms");
        public readonly string PUBLIC_PARTITION = "PUBLIC";
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Directory.CreateDirectory(realmDir);

            realmApp = App.Create("<APP-ID>");

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            AndroidX.Fragment.App.FragmentManager fm = this.SupportFragmentManager;

            var fragmentTransaction = fm.BeginTransaction();
            if (realmApp.CurrentUser == null)
            {
                fragmentTransaction.Replace(Resource.Id.MainFragmentContainerView, new SigninFragment());
            }
            else
            {
                fragmentTransaction.Replace(Resource.Id.MainFragmentContainerView, new TopFragment());
            }
            fragmentTransaction.AddToBackStack(null);
            fragmentTransaction.Commit();
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (View.IOnClickListener)null).Show();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
