﻿using System;
using System.IO;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using AndroidX.AppCompat.App;
using Realms;
using Realms.Sync;
using CreationLine.SampleApp.BasicRealmOperation;

namespace SampleApp
{
    public class AddFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View layout = inflater.Inflate(Resource.Layout.add, container, false);
            return layout;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            // 戻るボタンを押したときにトップ画面に遷移する
            Button returnButton = view.FindViewById<Button>(Resource.Id.ReturnAddInventoryButton);
            returnButton.Click += (sender, e) => Utils.ChangeFragment(this.ParentFragmentManager, new TopFragment());

            // 追加ボタンを押したときに商品が追加され、トップ画面に遷移する
            Button addInventoryButton = view.FindViewById<Button>(Resource.Id.AddInventoryButton);
            addInventoryButton.Click += AddInventory;
        }

        protected async void AddInventory(object sender, EventArgs e)
        {
            View view = this.View;
            ((Button)sender).Enabled = false;

            EditText itemText = view.FindViewById<EditText>(Resource.Id.AddItemEditText);
            EditText priceText = view.FindViewById<EditText>(Resource.Id.AddPriceEditText);
            EditText discountText = view.FindViewById<EditText>(Resource.Id.AddDiscountRateEditText);

            if (!Utils.ValidateForm(out int price, out int discount, sender, itemText, priceText, discountText)) { return; }

            try
            {
                App app = ((MainActivity)this.Activity).realmApp;

                using Realm publicRealm = await Realm.GetInstanceAsync(
                    new FlexibleSyncConfiguration(app.CurrentUser, "public.realm"));

                // 商品情報を追加する
                publicRealm.Write(() =>
                   publicRealm.Add(new Inventory()
                   {
                       Item = itemText.Text,
                       Partition = ((MainActivity)this.Activity).PUBLIC_PARTITION,
                       Price = price,
                       DiscountRate = discount,
                   }));
            }
            catch
            {
                Utils.ShowPopup(this.Activity, "商品の追加に失敗しました", (sender, e) => { });
                ((Button)sender).Enabled = true;
                return;
            }

            Utils.ShowPopup(this.Activity, "商品が追加できました", (sender, e) => Utils.ChangeFragment(this.ParentFragmentManager, new TopFragment()));
        }
    }
}