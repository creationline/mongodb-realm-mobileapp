﻿using System;
using System.IO;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using AndroidX.AppCompat.App;
using Realms;
using Realms.Sync;
using CreationLine.SampleApp.BasicRealmOperation;
using MongoDB.Bson;

namespace SampleApp
{
    public class EditFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.edit, container, false);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            var arg = base.Arguments;
            var mongoId = arg.GetString("mongo_id");
            var item = arg.GetString("item");
            var price = arg.GetString("price");
            var discountRate = arg.GetString("discount_rate");

            EditText itemEditText = view.FindViewById<EditText>(Resource.Id.EditInventoryItemEditText);
            itemEditText.Text = $"{item}";
            EditText priceEditText = view.FindViewById<EditText>(Resource.Id.EditInventoryPriceEditText);
            priceEditText.Text = $"{price}";
            EditText discountEditText = view.FindViewById<EditText>(Resource.Id.EditInventoryDiscountRateEditText);
            discountEditText.Text = $"{discountRate}";

            // 更新ボタンを押したときに商品が更新されて、トップ画面に遷移する
            Button applyInventoryButton = view.FindViewById<Button>(Resource.Id.ApplyInventoryButton);
            applyInventoryButton.Click += ApplyInventory;

            // 削除ボタンを押したときに商品をリストから削除し、トップ画面に遷移する
            Button deleteInventoryButton = view.FindViewById<Button>(Resource.Id.DeleteInventoryButton);
            deleteInventoryButton.Click += DeleteInventory;

            // 戻るボタンを押したときにトップ画面に遷移する
            Button returnButton = view.FindViewById<Button>(Resource.Id.ReturnEditInventoryButton);
            returnButton.Click += (sender, e) => Utils.ChangeFragment(this.ParentFragmentManager, new TopFragment());
        }

        protected async void ApplyInventory(object sender, EventArgs e)
        {
            View view = this.View;
            ((Button)sender).Enabled = false;

            EditText itemText = view.FindViewById<EditText>(Resource.Id.EditInventoryItemEditText);
            EditText priceText = view.FindViewById<EditText>(Resource.Id.EditInventoryPriceEditText);
            EditText discountText = view.FindViewById<EditText>(Resource.Id.EditInventoryDiscountRateEditText);

            if (!Utils.ValidateForm(out int price, out int discount, sender, itemText, priceText, discountText)) { return; }

            try
            {
                App app = ((MainActivity)this.Activity).realmApp;

                using Realm publicRealm = await Realm.GetInstanceAsync(
                    new FlexibleSyncConfiguration(app.CurrentUser, "public.realm"));

                // IDから商品情報を取得し、内容を更新する
                Inventory inventory = publicRealm.Find<Inventory>(primaryKey: ObjectId.Parse(base.Arguments.GetString("mongo_id")));
                publicRealm.Write(() =>
                {
                    inventory.Item = itemText.Text;
                    inventory.Price = price;
                    inventory.DiscountRate = discount;
                    inventory.Timestamp = DateTimeOffset.Now;
                });
            }
            catch
            {
                Utils.ShowPopup(this.Activity, "商品の変更に失敗しました", (sender, e) => { });
                ((Button)sender).Enabled = true;
                return;
            }

            Utils.ShowPopup(this.Activity, "商品の変更ができました", (sender, e) => Utils.ChangeFragment(this.ParentFragmentManager, new TopFragment()));
        }

        protected void DeleteInventory(object sender, EventArgs e)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.Activity);

            builder.SetTitle("本当に消しちゃっていいの..");

            builder.SetPositiveButton("YES", async (sender, e) =>
            {
                App app = ((MainActivity)this.Activity).realmApp;

                using Realm publicRealm = await Realm.GetInstanceAsync(
                    new FlexibleSyncConfiguration(app.CurrentUser, "public.realm"));

                Inventory inventory = publicRealm.Find<Inventory>(primaryKey: ObjectId.Parse(base.Arguments.GetString("mongo_id")));

                publicRealm.Write(() => publicRealm.Remove(inventory));

                AlertDialog.Builder deletebuilder = new AlertDialog.Builder(this.Activity);
                deletebuilder.SetTitle("商品を削除しました");
                deletebuilder.SetPositiveButton("OK", (sender, e) => Utils.ChangeFragment(this.ParentFragmentManager, new TopFragment()));
                deletebuilder.Show();
            });

            builder.SetNegativeButton("NO", (sender, e) => { });
            builder.Show();
        }
    }
}